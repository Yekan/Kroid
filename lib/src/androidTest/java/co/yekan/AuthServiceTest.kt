package co.yekan

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import android.util.Log
import co.yekan.kroid.entities.TokenResponse
import co.yekan.kroid.extentions.KHttpFailed
import co.yekan.kroid.services.KServiceConfig
import co.yekan.kroid.services.user.AuthService
import co.yekan.kroid.services.user.ILoginCallBack
import co.yekan.kroid.services.user.IRegisterCallBack
import co.yekan.kroid.services.user.auth.AuthConfig
import co.yekan.kroid.services.user.auth.setAuthConfig
import org.junit.*
import org.junit.runner.RunWith
import java.util.*
import java.util.concurrent.CountDownLatch

/**
 * Created by Mo3in on 10/1/2017.
 */
@RunWith(AndroidJUnit4::class)
class AuthServiceTest {

    companion object {
        private lateinit var email: String;
        private lateinit var pass: String;

        @BeforeClass
        @JvmStatic
        fun before() {
            email = "test_user${Date().time}@yahoo.com";
            pass = "P4ssw0rd^&";

            KServiceConfig.setAuthConfig(AuthConfig.Chakavak("roclient.public"));
        }
    }


    @Test
    fun Register() {
        val cd = CountDownLatch(1)
        Log.i("myLog", email + "," + pass)
        AuthService(InstrumentationRegistry.getTargetContext()).register(email, pass, object : IRegisterCallBack {
            override fun onFailed(failed: KHttpFailed) {
                Assert.fail(failed.message)
                cd.countDown()
            }

            override fun onSuccess() {
                Assert.assertTrue(true)
                cd.countDown()
            }

        })
        cd.await()
    }

    @Test
    fun Login() {
        val cd = CountDownLatch(1)
        Log.i("myLog", email + "," + pass)
        AuthService(InstrumentationRegistry.getTargetContext()).login( email, pass, object : ILoginCallBack {
            override fun onFailed(failed: KHttpFailed) {
                Assert.fail(failed.message)
                cd.countDown()
            }

            override fun onSuccess(tokenResponse: TokenResponse) {
                Assert.assertTrue(true)
                cd.countDown()
            }

        })
        cd.await()
    }

    @Test
    fun isLogin() {
        Assert.assertTrue(AuthService(InstrumentationRegistry.getTargetContext()).isLogin())
    }
}