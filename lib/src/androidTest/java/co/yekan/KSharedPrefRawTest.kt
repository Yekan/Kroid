package co.yekan

import android.content.Context
import org.junit.runner.RunWith
import org.junit.Test
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import co.yekan.kroid.storage.KSharedPrefRaw
import org.junit.Assert

/**
 * Created by Mo3in on 9/29/2017.
 */
@RunWith(AndroidJUnit4::class)
class KSharedPrefRawTest {
    val kShared = KSharedPrefRaw(InstrumentationRegistry.getTargetContext())

    @Test
    fun dataString() {
        kShared.set("string", "mo3in").apply()

        Assert.assertEquals("mo3in", kShared.get("string"))
    }


    @Test
    fun dataInt() {
        kShared.set("int", 123456).apply()

        Assert.assertEquals(123456, kShared.get<Int>("int"))
    }

    @Test
    fun dataBool() {
        kShared.set("bool", true).apply()

        Assert.assertEquals(true, kShared.get<Boolean>("bool"))
    }

    @Test
    fun dataModel() {
        val user = User("mo3in", "hente")
        kShared.set("model", user).apply()

        Assert.assertEquals(user, kShared.get<User>("model"))
    }

    data class User(var name: String, var family: String)
}