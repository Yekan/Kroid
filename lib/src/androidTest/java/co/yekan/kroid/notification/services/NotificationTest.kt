package co.yekan.kroid.notification.services

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Mo3in on 10/28/2017.
 */
@RunWith(AndroidJUnit4::class)
class NotificationTest {

    @Test
    fun notificationDefault() {
        val notif = KNotification.with(InstrumentationRegistry.getTargetContext());

        notif.setSmallIcon(android.R.drawable.ic_input_add)
                .setContentTitle("ok google")
                .setContentText("fekr mikonam hame chi khob dare pish mire ...")

        notif.notify(1)
    }
}