package co.yekan.kroid.entities

import co.yekan.kroid.services.KServiceConfig

/**
 * Created by Mo3in on 12/26/2017.
 */
data class FilePartialViewModel(
        val id: Int,
        val fullPath: String,
        val type: String,
        val description: String
) : IBaseEntity {
    fun getFinalPath(baseUrl: String? = null): String {
        if (baseUrl == null)
            return KServiceConfig.getHttpEndPoint() + fullPath
        return baseUrl + fullPath
    }
}