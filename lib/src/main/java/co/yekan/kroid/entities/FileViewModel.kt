package co.yekan.kroid.entities

/**
 * Created by Mo3in on 11/25/2017.
 */
data class FileViewModel(
        val id: Int,

        val fileName: String,
        val physicalName: String,

        val path: String,

        val length: Long,
        val description: String,

        val directoryId: Int,

        val createTime: String,
        val updateTime: String
) : IBaseEntity