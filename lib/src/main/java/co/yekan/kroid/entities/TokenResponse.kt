package co.yekan.kroid.entities

/**
 * Created by Mo3in on 12/14/2017.
 */
data class TokenResponse(val access_token: String, val expires_in: Long, val token_type: String, val refresh_token: String) : IBaseEntity