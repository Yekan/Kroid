package co.yekan.kroid.entities

/**
 * Created by Mo3in on 12/17/2017.
 */
 class UserEditModel(
        val about: String?,
        val birthDay:String?,
        name: String?,
        family: String?
): UserPartialEditModel(name, family)