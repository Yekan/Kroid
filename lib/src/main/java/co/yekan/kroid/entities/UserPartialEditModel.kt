package co.yekan.kroid.entities

/**
 * Created by Mo3in on 12/17/2017.
 */
open class UserPartialEditModel(
        val name :String?,
        val family :String?
):IBaseEntity