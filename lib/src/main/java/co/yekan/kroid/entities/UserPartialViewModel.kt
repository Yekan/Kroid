package co.yekan.kroid.entities

/**
 * Created by Mo3in on 12/26/2017.
 */
data class UserPartialViewModel(
        val id: Int,
        val userName: String,
        val email: String,
        val name: String,
        val family: String,
        val imageId: Int?,
        val image: FilePartialViewModel?
) : IBaseEntity {
    fun fullName(): String = name + " " + family
}