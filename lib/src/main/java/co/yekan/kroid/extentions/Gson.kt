package co.yekan.kroid.extentions

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by Mo3in on 12/20/2017.
 */
inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object: TypeToken<T>() {}.type)
