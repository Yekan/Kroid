package co.yekan.kroid.extentions

import android.content.Context
import co.yekan.kroid.ui.toast
import com.google.gson.Gson
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.HashMap

/**
 * Created by Mo3in on 9/13/2017.
 */

fun <T> Call<T>.run(success: (result: T) -> Unit, failed: ((failed: KHttpFailed) -> Unit)? = null, complete: ((response: Response<T>?, Throwable?) -> Unit)? = null) {
    this.enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>?, response: Response<T>) {

            if (response.isSuccessful) {
                success(response.body()!!)
                complete?.invoke(response, null)
                return
            }

            var error = ResponseError()
            var errorType = ResponseErrorType.Unknown

            if (response.code() == 400) {
                errorType = ResponseErrorType.BadRequest

                if (response.errorBody() != null) {
                    val errorBody = response.errorBody()!!.string()
                    if (errorBody.isNotEmpty())
                        error = Gson().fromJson<ResponseError>(errorBody)
                }
            }

            if (response.code() == 401 || response.code() == 403)
                errorType = ResponseErrorType.UnAuthorize

            if (response.code() == 500)
                errorType = ResponseErrorType.ServerError


            failed?.invoke(KHttpFailed(response.code(), errorType, error, response.message()))
            complete?.invoke(response, null)
        }

        override fun onFailure(call: Call<T>?, t: Throwable?) {
            failed?.invoke(KHttpFailed(0, ResponseErrorType.Unknown, null, t?.message))
            complete?.invoke(null, t)
        }
    })
}


class KHttpFailed(
        val statusCode: Int,
        val errorType: ResponseErrorType,
        val error: ResponseError? = null,
        val message: String? = null
) {
    fun errorAsString(): String = error?.toSeparateRows() ?: ""

    fun showAsToast(context: Context) {
        var message = message
        if (errorType == ResponseErrorType.BadRequest)
            message = errorAsString()

        if (message != null)
            context.toast(message)
    }
}

enum class ResponseErrorType {
    BadRequest,
    UnAuthorize,
    ServerError,
    Unknown
}

class ResponseError : HashMap<Any?, Array<String>>() {
    fun toSeparateRows(): String {
        if (this.size == 0) return ""

        var errorMsg = "";

        this.map { entry ->
            var inlineError = "";
            entry.value.map { s -> inlineError += s + "\n" }

            errorMsg += errorMsg + inlineError + "\n"
        }
        errorMsg = errorMsg.removeSuffix("\n").removeSuffix("\n")
        return errorMsg
    }
}