package co.yekan.kroid.extentions

import android.widget.ImageView
import co.yekan.kroid.entities.FilePartialViewModel
import co.yekan.kroid.helpers.glide.GlideApp
import com.bumptech.glide.load.engine.DiskCacheStrategy


/**
 * Created by Mo3in on 11/18/2017.
 */
fun ImageView.loadImage(src: String, cache: Boolean = true, autoCrop: Boolean = true) {
    var glide = GlideApp.with(context).load(src)

    if (!cache) {
        glide = glide.diskCacheStrategy(DiskCacheStrategy.NONE)
    }

    if (autoCrop)
        glide = glide.centerCrop()

    glide.into(this)
}

fun ImageView.loadImage(src: FilePartialViewModel?, cache: Boolean = true, autoCrop: Boolean = true) {
    if (src == null)
        return
    this.loadImage(src.getFinalPath(), cache, autoCrop)
}

