package co.yekan.kroid.extentions

import android.util.Log
import co.yekan.kroid.storage.KSharedPrefRaw
import com.google.gson.Gson

/**
 * Created by Mo3in on 12/16/2017.
 */
inline fun <reified T> log(tag: String, value: T) {
    when (T::class.java) {
        Int::class.java,
        String::class.java,
        Boolean::class.java,
        Long::class.java,
        Float::class.java -> Log.i(tag, value.toString())
        else -> Log.i(tag, Gson().toJson(value))
    }
}

inline fun <reified T> log(value: T) {
    log("Kroid", value)
}
