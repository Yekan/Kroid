package co.yekan.kroid.extentions

import android.os.AsyncTask
import android.os.Handler
import android.support.v7.widget.SearchView
import android.widget.ArrayAdapter
import co.yekan.kroid.ui.widget.KRecyclerAdapter

/**
 * Created by Mo3in on 2/19/2018.
 */
fun <T> ArrayAdapter<T>.RestAdapter(searchView: SearchView, timeOut: Long, job: (text: String) -> Array<T>) {

    searchView.setRestAdapterFilter({ text ->

        if (!text.isNullOrEmpty()) {
            RetrieveData(job, { items ->
                clear()
                addAll(items.toMutableList())
                notifyDataSetChanged()
            }).execute(text)
        }
    })

}

fun <T> ArrayAdapter<T>.RestAdapter(searchView: SearchView, job: (text: String) -> Array<T>) {
    RestAdapter(searchView, 300L, job)
}

fun <T : Any> KRecyclerAdapter<T>.RestAdapter(searchView: SearchView, timeOut: Long, job: (text: String) -> Array<T>) {

    searchView.setRestAdapterFilter({ text ->

        if (!text.isNullOrEmpty())
            RetrieveData(job, { items -> setItems(items) }).execute(text)
    })
}

fun <T : Any> KRecyclerAdapter<T>.RestAdapter(searchView: SearchView, job: (text: String) -> Array<T>) {
    RestAdapter(searchView, 300L, job)
}

class RetrieveData<T>(private var job: (text: String) -> Array<T>, private var onResult: (items: Array<T>) -> Unit) : AsyncTask<String, Void, Array<T>>() {
    override fun doInBackground(vararg params: String?): Array<T> = job.invoke(params[0].toString())

    override fun onPostExecute(result: Array<T>) {
        onResult.invoke(result)
    }
}

private fun SearchView.setRestAdapterFilter(onNewText: (text: String?) -> Unit) {
    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            onNewText.invoke(query)
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            onNewText.invoke(newText)
            return false
        }
    })
}