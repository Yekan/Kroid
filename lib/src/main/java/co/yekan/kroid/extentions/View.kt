package co.yekan.kroid.extentions

import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import com.google.gson.Gson

/**
 * Created by Mo3in on 1/14/2018.
 */
fun View.valueAsString(): String {
    return when (this) {
        is EditText -> return text.toString()
        is TextInputLayout -> return editText?.text.toString()
        is Spinner -> return Gson().toJson(selectedItem)
        else -> ""
    }
}

fun View.isEditText(): Boolean = when (this) {
    is EditText -> true
    is TextInputLayout -> true
    else -> false
}

fun View.setError(messsage: String) {
    when (this) {
        is TextInputLayout -> {
            this.isErrorEnabled = true
            this.error = messsage
        }
    }
}

fun View.removeError() {
    when (this) {
        is TextInputLayout -> {
            this.error = ""
        }
    }
}