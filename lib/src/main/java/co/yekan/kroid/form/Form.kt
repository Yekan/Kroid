package co.yekan.kroid.form

import android.view.View
import co.yekan.kroid.extentions.fromJson
import co.yekan.kroid.extentions.log
import co.yekan.kroid.extentions.removeError
import co.yekan.kroid.extentions.valueAsString
import co.yekan.kroid.ui.toast
import com.google.gson.Gson
import com.google.gson.JsonObject


/**
 * Created by Mo3in on 1/13/2018.
 */
class Form(private val items: Map<View, ElementHolder>, val modelBinder: HashMap<String, ElementBinderHolder>) {

    class Builder {
        private var validators: HashMap<View, ElementHolder> = hashMapOf()
        private var modelBinder: HashMap<String, ElementBinderHolder> = hashMapOf()

        fun setElement(view: View, title: String, vararg rules: Validator): Builder {
            val holder = ElementHolder(title, rules.asList())
            rules.forEach { validator ->
                validator.view = view
                validator.onInit(view)
            }

            validators[view] = holder

            return this
        }

        fun setElement(view: View, vararg rules: Validator): Builder = setElement(view, "", *rules)

        fun <TView> setElement(propName: String, title: String, view: TView, mapper: ((view: TView) -> Any)? = null, vararg rules: Validator): Builder where TView : View {
            modelBinder.put(propName, ElementBinderHolder(view, if (mapper == null) defaultBinder(view) else mapper as ((view: View) -> Any)))
            return setElement(view, title, *rules)
        }

        fun setElement(propName: String, view: View, vararg rules: Validator): Builder {
            modelBinder.put(propName, ElementBinderHolder(view, defaultBinder(view)))
            return setElement(view, *rules)
        }

        private fun defaultBinder(view: View): (view: View) -> Any = { itemView -> itemView.valueAsString() }


        fun build(): Form = Form(validators, modelBinder)
    }


    private fun checkValidators(): ArrayList<ValidatorResult> {
        val failedValidators = ArrayList<ValidatorResult>()

        for (validator in items) {
            validator.key.removeError()

            validator.value.validators.forEach { iValidator ->
                if (!iValidator.isValid())
                    failedValidators.add(ValidatorResult(validator.key, validator.value.title, iValidator))
            }
        }
        return failedValidators
    }

    fun isValid(): Boolean = checkValidators().size == 0

    inline fun <reified TModel> getModel(): TModel {
        val obj = JsonObject()
        for (mutableEntry in modelBinder)
            obj.add(mutableEntry.key, Gson().toJsonTree(mutableEntry.value.mapper?.invoke(mutableEntry.value.view)))

        return Gson().fromJson<TModel>(obj.toString())
    }

    fun isValidAndShowErrors(): Boolean {
        val validators = checkValidators()
        if (validators.size == 0) return true

        validators.forEach { validator -> validator.validator.showError() }

        val first = validators.first()
        first.view.context.toast(first.title + ": " + first.validator.errMessage)
        log(first.title + ": " + first.validator.errMessage)

        return false
    }
}

data class ElementHolder(val title: String, val validators: List<Validator>)
data class ValidatorResult(val view: View, val title: String, val validator: Validator)
data class ElementBinderHolder(val view: View, val mapper: ((view: View) -> Any)?)