### فرم ساز

وضایف:
- بررسی شروط برای المنت ها
- بازگرداندن مدل

نحوه استفاده:
```kotlin
val form = Form.Builder()
                 .setElement("name", txt_name, Validator.maxLength(20), Validator.required())
                 .setElement("family", txt_family, Validator.maxLength(25), Validator.required())
                 .setElement("genderId", "جنسیت", spinner_gender, { (it.selectedItem as Gender).id }, Validator.required())
                 .setElement("address", txt_address, Validator.required())
                 .build()
```

برای بررسی فرم از تابع `isValid` خروجی `form` استفاده میشود

برای گرفتن مدل فرم از تابع `getModel` به صورت زیر استفاده میکنیم:
```kotlin
form.getModel<UserEditModel>()
```