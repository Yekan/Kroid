package co.yekan.kroid.form

import android.view.View
import co.yekan.kroid.form.rules.MaxLength
import co.yekan.kroid.form.rules.Required
import co.yekan.kroid.extentions.setError

/**
 * Created by Mo3in on 1/13/2018.
 */
abstract class Validator {
    private var _view: View? = null

    var view: View
        set(value) {
            _view = value
        }
        get() = _view!!

    abstract fun isValid(): Boolean

    open fun onInit(view: View) {

    }

    open fun showError() {
        view.setError(errMessage)
    }

    abstract val errMessage: String


    companion object {
        fun maxLength(maxLength: Int): MaxLength = MaxLength(maxLength)
        fun required(): Required = Required()
    }
}

