package co.yekan.kroid.form.rules

import android.support.design.widget.TextInputLayout
import android.view.View

import co.yekan.kroid.extentions.valueAsString
import co.yekan.kroid.form.Validator

/**
 * Created by Mo3in on 1/13/2018.
 */
class MaxLength(private val maxLength: Int) : Validator() {
    override fun isValid(): Boolean = view.valueAsString().length <= maxLength

    override fun onInit(view: View) {
        super.onInit(view)
        when (view) {
            is TextInputLayout -> {
                if (view.isCounterEnabled)
                    view.counterMaxLength = maxLength
            }
        }
    }

    override val errMessage: String
        get() = "max length must be $maxLength"

}