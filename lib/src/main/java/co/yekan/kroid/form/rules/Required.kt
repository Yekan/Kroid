package co.yekan.kroid.form.rules

import android.widget.Spinner
import co.yekan.kroid.extentions.isEditText
import co.yekan.kroid.extentions.valueAsString
import co.yekan.kroid.form.Validator

/**
 * Created by Mo3in on 1/13/2018.
 */
class Required : Validator() {
    override fun isValid(): Boolean {
        if (view.isEditText())
            return view.valueAsString().isNotEmpty()

        return when (view) {
            is Spinner -> (view as Spinner).selectedItemPosition >= 0
            else -> false
        }
    }


    override val errMessage: String = "اجباری ..."
}