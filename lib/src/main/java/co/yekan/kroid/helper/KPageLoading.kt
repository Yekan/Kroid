package co.yekan.kroid.helper

import android.app.Dialog
import android.view.View
import co.yekan.kroid.ui.KDialog.KProgressDialog

/**
 * Created by Mo3in on 2/26/2018.
 */
fun View.setLoadProcess(message: String = "درحال دریافت اطلاعات ..."): KPageLoading {
    visibility = View.GONE
    val dialog = KProgressDialog.show(context, "", message)
    return KPageLoading(this, dialog)
}

class KPageLoading(private val view: View, private val progressDialog: Dialog) {
    fun dismiss() {
        view.visibility = View.VISIBLE
        progressDialog.dismiss()
    }
}