package co.yekan.kroid.helpers.glide

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import co.yekan.kroid.helpers.glide.svg.SvgDecoder
import com.caverock.androidsvg.SVG
import android.graphics.drawable.PictureDrawable
import co.yekan.kroid.helpers.glide.svg.SvgDrawableTranscoder
import java.io.InputStream


@GlideModule
class KAppGlideModule : AppGlideModule(){
    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        super.registerComponents(context, glide, registry)

        registry.register(SVG::class.java, PictureDrawable::class.java, SvgDrawableTranscoder())
                .append(InputStream::class.java, SVG::class.java, SvgDecoder())
    }
}