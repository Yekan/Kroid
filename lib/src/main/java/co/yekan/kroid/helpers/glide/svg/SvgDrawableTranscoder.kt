package co.yekan.kroid.helpers.glide.svg

import com.bumptech.glide.load.resource.SimpleResource
import android.graphics.drawable.PictureDrawable
import android.graphics.Picture
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.engine.Resource
import com.caverock.androidsvg.SVG
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder


class SvgDrawableTranscoder : ResourceTranscoder<SVG, PictureDrawable> {
    override fun transcode(toTranscode: Resource<SVG>, options: Options): Resource<PictureDrawable> {
        val svg = toTranscode.get()
        val picture = svg.renderToPicture()
        val drawable = PictureDrawable(picture)
        return SimpleResource(drawable)
    }
}