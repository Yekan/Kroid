package co.yekan.kroid.messaging

import co.yekan.kroid.messaging.entities.MessagingTokenInsertModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Mo3in on 10/19/2017.
 */
interface MessagingApiService {
    @POST("api/NotifTokens/AddNotifToken")
    fun addNotificationToken(@Body notifTokenInsertModel: MessagingTokenInsertModel): Call<ResponseBody>
}