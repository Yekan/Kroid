package co.yekan.kroid.messaging.services

import android.util.Log
import co.yekan.kroid.extentions.log
import co.yekan.kroid.notification.NotificationAgentsManager
import co.yekan.kroid.notification.entities.INotificationAgent
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

/**
 * Created by Mo3in on 10/28/2017.
 */
abstract class BaseNotificationMessageService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        if (remoteMessage == null) {
            log("remote message is null")
            return
        }

        var agent: INotificationAgent? = null;
        if (remoteMessage.data != null && remoteMessage.data.containsKey("agentKey"))
            agent = NotificationAgentsManager.getHolder(remoteMessage.data["agentKey"]!!)!!

        if (agent == null)
            agent = NotificationAgentsManager.getDefaultAgent()

        agent.run(baseContext, remoteMessage)
    }

}