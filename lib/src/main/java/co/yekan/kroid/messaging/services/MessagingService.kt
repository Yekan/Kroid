package co.yekan.kroid.messaging.services

import android.content.Context
import co.yekan.kroid.extentions.run
import co.yekan.kroid.messaging.MessagingApiService
import co.yekan.kroid.messaging.entities.MessagingTokenInsertModel
import co.yekan.kroid.services.user.auth.AuthHttp
import co.yekan.kroid.ui.Toast
import com.google.firebase.iid.FirebaseInstanceId

/**
 * Created by Mo3in on 10/19/2017.
 */
class MessagingService {
    companion object {
        fun sendToken(context: Context, firebaseInstanceId: FirebaseInstanceId) {
            val token = firebaseInstanceId.token
            if (token == null) {
                Toast.show(context, "token is empty ...");
                return
            }
            AuthHttp.build<MessagingApiService>(context).addNotificationToken(MessagingTokenInsertModel(token)).run({ kResponse ->
            })
        }
    }
}