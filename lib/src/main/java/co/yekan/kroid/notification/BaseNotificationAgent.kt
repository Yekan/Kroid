package co.yekan.kroid.notification

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import co.yekan.kroid.notification.entities.INotificationAgent
import co.yekan.kroid.notification.services.KNotification
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson

/**
 * Created by Mo3in on 12/20/2017.
 */
abstract class BaseNotificationAgent<TModel>(private var _class: Class<TModel>) : INotificationAgent {
    lateinit var context: Context
    lateinit var remoteMessage: RemoteMessage

    lateinit var notif: KNotification

    override fun run(context: Context, remoteMessage: RemoteMessage) {
        if (!remoteMessage.data.containsKey("data"))
            return

        val data = Gson().fromJson<TModel>(remoteMessage.data["data"]!!, _class)
        this.context = context
        this.remoteMessage = remoteMessage


        this.notif = KNotification.with(context, channelId)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var channel = notif.notificationManager.getNotificationChannel(channelId)
            if (channel == null) {
                channel = defaultChannel
                notif.notificationManager.createNotificationChannel(channel)
            }
        }

        notif.setChannelId(channelId)

        run(data)
    }

    open val defaultChannel: NotificationChannel
        @TargetApi(Build.VERSION_CODES.O)
        get() {
            val chanel = NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT)
            with(chanel) {
                lightColor = Color.RED
            }
            return defaultChannel
        }

    open val channelDesc: String
        get() = "";

    open val channelName: String
        get() = channelId;

    abstract fun run(data: TModel)
}