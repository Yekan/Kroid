package co.yekan.kroid.notification

import android.content.Context
import co.yekan.kroid.notification.entities.INotificationAgent
import co.yekan.kroid.notification.services.KNotification
import co.yekan.kroid.services.KServiceConfig
import com.google.firebase.messaging.RemoteMessage


/**
 * Created by Mo3in on 10/28/2017.
 */
class DefaultNotificationAgent : INotificationAgent {
    companion object {
        val key = "default"
    }

    override fun run(context: Context, remoteMessage: RemoteMessage) {

        val notif = KNotification.with(context, channelId)

        notif.setSmallIcon(KServiceConfig.getNotificationDefaultIcon())


        if (remoteMessage.notification!!.title != null)
            notif.setContentTitle(remoteMessage.notification!!.title)

        if (remoteMessage.notification!!.body != null)
            notif.setContentText(remoteMessage.notification!!.body)


        notif.notify(1)
    }


    override val channelId: String
        get() = DefaultNotificationAgent.key

}