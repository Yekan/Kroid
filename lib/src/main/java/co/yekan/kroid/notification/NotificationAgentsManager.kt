package co.yekan.kroid.notification

import co.yekan.kroid.notification.entities.INotificationAgent
import java.util.ArrayList

/**
 * Created by Mo3in on 10/28/2017.
 */
class NotificationAgentsManager {
    companion object {
        private var holders = ArrayList<INotificationAgent>()

        fun addHolder(notificationHolder: INotificationAgent) {
            holders.add(notificationHolder)
        }

        fun getDefaultAgent(): INotificationAgent = holders.first { x -> x.channelId == DefaultNotificationAgent.key }

        fun getHolder(key: String, useDefault: Boolean = true): INotificationAgent? {
            var agent = holders.firstOrNull { x -> x.channelId == key }

            if (agent == null && useDefault)
                agent = getDefaultAgent()

            return agent
        }

        init {
            addHolder(DefaultNotificationAgent())
        }
    }
}