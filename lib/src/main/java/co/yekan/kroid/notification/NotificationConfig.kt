package co.yekan.kroid.notification

import co.yekan.kroid.services.KServiceConfig

/**
 * Created by Mo3in on 10/28/2017.
 */

fun KServiceConfig.setNotificationDefaultIcon(resId: Int): KServiceConfig {
    this.config.put("notification.default.icon", resId)
    return this;
}

fun KServiceConfig.getNotificationDefaultIcon(): Int = config["notification.default.icon"] as Int