package co.yekan.kroid.notification.entities

import android.content.Context
import com.google.firebase.messaging.RemoteMessage

/**
 * Created by Mo3in on 10/28/2017.
 */
interface INotificationAgent {
    fun run(context: Context, remoteMessage: RemoteMessage);
    val channelId: String;
}