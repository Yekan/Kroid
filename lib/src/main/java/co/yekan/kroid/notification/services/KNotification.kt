package co.yekan.kroid.notification.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.support.v4.app.NotificationCompat
import co.yekan.kroid.notification.getNotificationDefaultIcon
import co.yekan.kroid.services.KServiceConfig


/**
 * Created by Mo3in on 10/28/2017.
 */
class KNotification(val context: Context, val channelId: String) : NotificationCompat.Builder(context, channelId) {
    companion object {
        fun with(context: Context, channelId: String): KNotification = KNotification(context.applicationContext, channelId)
    }

    val notificationManager: NotificationManager;

    init {
        setDefaults(android.app.Notification.DEFAULT_ALL)
        setSmallIcon(KServiceConfig.getNotificationDefaultIcon())

        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val privateMessagesChannel = NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT)

            notificationManager.createNotificationChannel(privateMessagesChannel)
        }
    }

    fun notify(id: Int) {
        notificationManager.notify(id, build())
    }
}