package co.yekan.kroid.permissionRequest

import android.annotation.SuppressLint

/**
 * Created by Mo3in on 12/10/2017.
 */
interface IPermissionCallBack {
    @SuppressLint("MissingPermission")
    fun permissionGranted()

    fun permissionDenied()
}