package co.yekan.kroid.services.KHttpClient

import co.yekan.kroid.services.KServiceConfig
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Mo3in on 12/26/2017.
 */
class KHttpClient {
    companion object {
        inline fun <reified T> build(basePath: String = ""): T {
            val builder = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())

            if (!basePath.isEmpty())
                builder.baseUrl(basePath)
            else
                builder.baseUrl(KServiceConfig.getHttpEndPoint())

            return builder.build().create(T::class.java)
        }

        inline fun <reified T> buildRx(basePath: String = ""): T {
            val builder = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())

            if (!basePath.isEmpty())
                builder.baseUrl(basePath)
            else
                builder.baseUrl(KServiceConfig.getHttpEndPoint())

            return builder.build().create(T::class.java)
        }
    }
}