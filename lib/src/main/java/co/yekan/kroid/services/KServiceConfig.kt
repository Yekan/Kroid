package co.yekan.kroid.services

import android.content.Context
import java.util.*

/**
 * Created by Mo3in on 9/12/2017.
 */
object KServiceConfig {
    var config = HashMap<String, Any>();

    private val HTTP_END_POINT = "http_endpoint"
    private val DEFAULT_LANG = "default_lang"

    fun setHttpEndPoint(url: String): KServiceConfig {
        config[HTTP_END_POINT] = url;
        return this
    }

    fun getHttpEndPoint(): String = config[HTTP_END_POINT] as String


    fun setDefaultLang(lang: String): KServiceConfig {
        config[DEFAULT_LANG] = lang;
        return this
    }

    fun getDefaultLang(): String {
        if (config.containsKey(DEFAULT_LANG))
            return config[DEFAULT_LANG].toString()
        return "en"
    }
}