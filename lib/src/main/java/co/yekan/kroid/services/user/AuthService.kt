package co.yekan.kroid.services.user


import android.content.Context
import co.yekan.kroid.entities.FileViewModel
import co.yekan.kroid.entities.IBaseEntity
import co.yekan.kroid.entities.TokenResponse
import co.yekan.kroid.entities.UserPartialEditModel
import co.yekan.kroid.extentions.*
import co.yekan.kroid.services.KServiceConfig
import co.yekan.kroid.services.user.auth.AuthHttp
import co.yekan.kroid.services.user.auth.IAuthHttpService
import co.yekan.kroid.services.user.auth.getAuthConfig
import co.yekan.kroid.storage.KSharedPrefRaw
import com.auth0.android.jwt.JWT
import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.*
import java.io.File
import java.io.IOException
import java.net.URL
import java.util.*


/**
 * Created by Mo3in on 9/11/2017.
 */
class AuthService(val context: Context) {
	companion object {
		private val KeyUserAuth = "user.login.auth"
		val KeyUserInfo = "user.info"
	}

	private fun httpClient(): OkHttpClient = OkHttpClient.Builder().protocols(Collections.singletonList(Protocol.HTTP_1_1)).build()

	fun login(userName: String, password: String, callback: ILoginCallBack) {
		val config = KServiceConfig.getAuthConfig()

		val client = httpClient()

		val jsonObject = JsonObject()

		jsonObject.addProperty("username", userName)
		jsonObject.addProperty("password", password)

		if (config.client_id != null) jsonObject.addProperty("client_id", config.client_id)
		if (config.grant_type != null) jsonObject.addProperty("grant_type", config.grant_type)

		val formBuilder = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString())


		val request = Request.Builder().url(config.getLoginEndPoint()).post(formBuilder).build()

		client.newCall(request).enqueue(object : Callback {
			override fun onResponse(call: Call?, response: Response?) {
				val responseData = response?.body()!!.string()

				if (response.isSuccessful) {
					val token = Gson().fromJson(responseData, TokenResponse::class.java)

					KSharedPrefRaw(context).set(KeyUserAuth, token).apply();

					callback.onSuccess(token)
				} else if (response.code() == 400) {
					callback.onFailed(KHttpFailed(response.code(), ResponseErrorType.BadRequest, Gson().fromJson<ResponseError>(response.body()?.string()!!)))
				} else callback.onFailed(KHttpFailed(response.code(), ResponseErrorType.Unknown, null, ""))
			}

			override fun onFailure(call: Call?, e: IOException?) {
				callback.onFailed(KHttpFailed(0, ResponseErrorType.Unknown, null, e?.message))
			}
		})
	}

	fun getToken(): TokenResponse? = KSharedPrefRaw(context).get(KeyUserAuth)

	fun getJwtToken(): JWT = JWT(getToken()!!.access_token)

	fun register(model: IRegisterModel, callBack: IRegisterCallBack) {
		val config = KServiceConfig.getAuthConfig()


		val formBuilder = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), Gson().toJson(model))


		val request = Request.Builder().url(URL(config.getRegisterEndPoint())).post(formBuilder).build()

		httpClient().newCall(request).enqueue(object : Callback {
			override fun onResponse(call: Call?, response: Response) {
				if (response.isSuccessful) callBack.onSuccess()
				else {
					if (response.code() == 400) {
						val error = Gson().fromJson<ResponseError>(response.body()?.string()!!)
						callBack.onFailed(KHttpFailed(400, ResponseErrorType.BadRequest, error))
					} else callBack.onFailed(KHttpFailed(response.code(), ResponseErrorType.Unknown, null, response.body()?.string() ?: response.message()))
				}
			}

			override fun onFailure(call: Call?, e: IOException?) {
				e?.printStackTrace()
				callBack.onFailed(KHttpFailed(0, ResponseErrorType.Unknown, null, if (e?.message == null) "" else e.message!!))
			}
		})
	}

	fun register(email: String, password: String, callBack: IRegisterCallBack) {
		register(RegisterModel(email, password, password), callBack)
	}

	fun isLogin(): Boolean = KSharedPrefRaw(context).hasKey(KeyUserAuth)

	fun requestTotp(phone: String, callBack: IToTpRequestCallBack) {

		val content = JsonObject()
		content.addProperty("phone", phone)
		val formBuilder = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), content.toString())

		val request = Request.Builder().url(KServiceConfig.getAuthConfig().getTotpRequestEndPoint()).post(formBuilder).build()

		httpClient().newCall(request).enqueue(object : Callback {
			override fun onFailure(call: Call?, e: IOException?) {
				callBack.onFailed(0, if (e?.message == null) "" else e.message!!)
			}

			override fun onResponse(call: Call?, response: Response) {
				if (response.isSuccessful) {
					val data = Gson().fromJson(response.body()?.string(), ToTpRequestResponse::class.java);
					callBack.onSuccess(data)
				} else {
					callBack.onFailed(response.code(), response.body()?.string() ?: response.message())
				}
			}

		})
	}

	fun loginTotp(phone: String, code: String, callBack: ILoginCallBack) {

		val content = JsonObject()

		content.addProperty("phone", phone)
		content.addProperty("code", code)

		val formBuilder = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), content.toString())

		val request = Request.Builder().url(KServiceConfig.getAuthConfig().getTotpLoginEndPoint()).post(formBuilder).build()

		httpClient().newCall(request).enqueue(object : Callback {
			override fun onFailure(call: Call?, e: IOException?) {
				callBack.onFailed(KHttpFailed(0, ResponseErrorType.Unknown, null, if (e?.message == null) "" else e.message!!))
			}

			override fun onResponse(call: Call?, response: Response) {
				val responseData = response.body()!!.string()

				if (response.isSuccessful) {
					val token = Gson().fromJson(responseData, TokenResponse::class.java)

					KSharedPrefRaw(context).set(KeyUserAuth, token).apply();

					callBack.onSuccess(token)
				} else if (response.code() == 400) {
					val error = Gson().fromJson<ResponseError>(response.body()?.string()!!)
					callBack.onFailed(KHttpFailed(response.code(), ResponseErrorType.BadRequest, error))
				} else callBack.onFailed(KHttpFailed(response.code(), ResponseErrorType.Unknown, null, response.body()?.string() ?: response.message()))
			}

		})
	}

	inline fun <reified T> getInfo(callback: IUserInfoCallBack<T>, showPreview: Boolean = true) where T : IBaseEntity {
		if (showPreview && KSharedPrefRaw(context).hasKey(AuthService.KeyUserInfo)) callback.onReceive(KSharedPrefRaw(context).get(AuthService.KeyUserInfo))

		val config = KServiceConfig.getAuthConfig()

		AuthHttp.build<IAuthHttpService>(context, config.endPoint).getInfo(config.userInfoPath!!).run({ responseBody ->
			val data = Gson().fromJson<T>(responseBody.string())

			KSharedPrefRaw(context).set(AuthService.KeyUserInfo, data).apply()
			callback.onReceive(data)
		}, { kHttpFail ->
			callback.onFailed(kHttpFail)
		})
	}

	inline fun <reified T> updateInfo(data: T, callback: IUserInfoUpdateCallBack<T>) where T : UserPartialEditModel {
		val config = KServiceConfig.getAuthConfig()
		AuthHttp.build<IAuthHttpService>(context, config.endPoint).setInfo(config.editUserInfoPath!!, data).run({ responseBody ->
			callback.onReceive(Gson().fromJson<T>(responseBody.string()))
		}, { kHttpFail ->
			callback.onFailed(kHttpFail)
		})
	}

	fun uploadAvatar(imageFile: File, callback: IProfileChangeAvatar) {
		if (!isLogin()) {
			callback.onFailed(KHttpFailed(0, ResponseErrorType.UnAuthorize))
			return
		}
		val config = KServiceConfig.getAuthConfig()

		val image = MultipartBody.Part.createFormData("image", imageFile.name, RequestBody.create(MediaType.parse("multipart/form-data"), imageFile))

		AuthHttp.build<IAuthHttpService>(context, config.endPoint).uploadAvatar(config.changeAvatarPath!!, image).run({ fileViewModel ->
			callback.onSuccess(fileViewModel)
		}, { kHttpFail ->
			callback.onFailed(kHttpFail)
		})
	}

	fun logOut() {
		KSharedPrefRaw(context).remove(KeyUserAuth).apply()
	}

	fun isInRole(name: String): Boolean {
		if (!isLogin()) return false
		val role = getJwtToken().getClaim(name).asList<String>(String::class.java).firstOrNull { x -> x == name }
		return !role.isNullOrEmpty()
	}
}

interface ILoginCallBack {
	fun onSuccess(tokenResponse: TokenResponse)
	fun onFailed(failed: KHttpFailed)
}

interface IRegisterCallBack {
	fun onSuccess()
	fun onFailed(failed: KHttpFailed)
}

interface IUserInfoCallBack<T> {
	fun onReceive(info: T)
	fun onFailed(failed: KHttpFailed)
}

interface IUserInfoUpdateCallBack<T> {
	fun onReceive(info: T)
	fun onFailed(failed: KHttpFailed)
}

interface IProfileChangeAvatar {
	fun onSuccess(image: FileViewModel)
	fun onFailed(failed: KHttpFailed)
}

interface IToTpRequestCallBack {
	fun onSuccess(data: ToTpRequestResponse)
	fun onFailed(statusCode: Int, message: String)
}

interface IRegisterModel {
	val email: String
	val password: String
	val confirmPassword: String
}

data class RegisterModel(override val email: String, override val password: String, override val confirmPassword: String) : IRegisterModel
data class ToTpRequestResponse(val number: String, val isRegistered: Boolean)