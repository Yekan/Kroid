package co.yekan.kroid.services.user.auth

import co.yekan.kroid.services.KServiceConfig

/**
 * Created by Mo3in on 9/12/2017.
 */

fun KServiceConfig.setAuthConfig(endPoint: AuthConfig): KServiceConfig {
    this.config.put("user.auth", endPoint)
    return this;
}

fun KServiceConfig.getAuthConfig(): AuthConfig = config["user.auth"] as AuthConfig

class AuthConfig(
        var endPoint: String,
        var loginPath: String? = null,
        var registerPath: String? = null,
        val userInfoPath: String? = null,
        val editUserInfoPath: String? = null,
        var totpRequestPath: String? = null,
        var totpLoginPath: String? = null,
        var changeAvatarPath: String? = null,
        var client_id: String? = null,
        var grant_type: String? = "password") {
    companion object {
        fun Chakavak(client_id: String): AuthConfig = AuthConfig(
                "http://accounts.chakavak.co/",
                "connect/token",
                registerPath = "api/Account/Register",
                userInfoPath = "api/Account/Info",
                changeAvatarPath = "api/Account/Profile",
                client_id = client_id
        )
        fun Kican(endPoint: String):AuthConfig = AuthConfig(
                endPoint,
                "api/Account/Login",
                "api/Account/Register",
                "api/Account/Info",
                "api/Account/Edit",
                "api/Account/RequestToTp",
                "api/Account/LoginToTp",
                "api/Account/SetProfile"
        )
    }

    fun getRegisterEndPoint(): String = this.endPoint + this.registerPath;
    fun getLoginEndPoint(): String = this.endPoint + this.loginPath
    fun getTotpLoginEndPoint(): String = this.endPoint + this.totpLoginPath
    fun getTotpRequestEndPoint(): String = this.endPoint + this.totpRequestPath
    fun getUserInfoEndPoint(): String = this.endPoint + this.userInfoPath
}