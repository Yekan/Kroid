package co.yekan.kroid.services.user.auth

import android.content.Context
import co.yekan.kroid.services.KServiceConfig
import co.yekan.kroid.services.user.AuthService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Mo3in on 9/11/2017.
 */
class AuthHttp {
    companion object {
        inline fun <reified T> build(context: Context, basePath: String? = null): T = instance(context, basePath, false).create(T::class.java)
        inline fun <reified T> buildRx(context: Context, basePath: String? = null): T = instance(context, basePath, true).create(T::class.java)

        fun instance(context: Context, basePath: String? = null, isRx: Boolean): Retrofit {

            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor { chain ->
                val original = chain.request()

                val authService = AuthService(context)

                if (!authService.isLogin()) {
                    // todo: goto login page ...

                }

                val builder = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .method(original.method(), original.body())
                val token = authService.getToken()
                if (token != null)
                    builder.header("Authorization", "Bearer " + token.access_token)
                val request = builder.build()

                chain.proceed(request)
            }

            val builder = Retrofit.Builder()

            if (!basePath.isNullOrEmpty())
                builder.baseUrl(basePath)
            else
                builder.baseUrl(KServiceConfig.getHttpEndPoint())

            if (isRx)
                builder.addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())

            builder.addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())

            return builder.build()
        }
    }


}