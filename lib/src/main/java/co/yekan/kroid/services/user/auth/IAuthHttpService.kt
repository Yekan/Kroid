package co.yekan.kroid.services.user.auth

import co.yekan.kroid.entities.FileViewModel
import co.yekan.kroid.entities.IBaseEntity
import co.yekan.kroid.entities.UserPartialEditModel
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by Mo3in on 11/25/2017.
 */
interface IAuthHttpService {
    @Multipart
    @POST
    fun uploadAvatar(@Url url: String, @Part image: MultipartBody.Part): Call<FileViewModel>

    @GET
    fun getInfo(@Url url: String): Call<ResponseBody>

    @POST
    fun setInfo(@Url url: String, @Body data: Any): Call<ResponseBody>
}
