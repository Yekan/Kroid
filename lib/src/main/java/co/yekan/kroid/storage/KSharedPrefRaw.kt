package co.yekan.kroid.storage

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson


/**
 * Created by Mo3in on 9/28/2017.
 */
class KSharedPrefRaw(context: Context, key: String = "Kroid") {
    val prefs: SharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);

    private var _editor: SharedPreferences.Editor? = null;

    fun editor(): SharedPreferences.Editor {
        if (_editor == null)
            _editor = prefs.edit();
        return _editor!!
    }

    inline fun <reified T> get(key: String, default: T? = null): T = when (T::class.java) {
        Int::class.java -> prefs.getInt(key, if (default == null) 0 else default as Int) as T
        String::class.java -> prefs.getString(key, if (default == null) "" else default as String) as T
        Boolean::class.java -> prefs.getBoolean(key, if (default == null) false else default as Boolean) as T
        Long::class.java -> prefs.getLong(key, if (default == null) 0 else default as Long) as T
        Float::class.java -> prefs.getFloat(key, if (default == null) 0f else default as Float) as T
        else -> {
            Gson().fromJson<T>(prefs.getString(key, if (default == null) "" else default as String), T::class.java)
        }
    }

    inline fun <reified T> set(key: String, value: T): KSharedPrefRaw {
        when (T::class.java) {
            Int::class.java -> editor().putInt(key, value as Int) as T
            String::class.java -> editor().putString(key, value as String)
            Boolean::class.java -> editor().putBoolean(key, value as Boolean) as T
            Long::class.java -> editor().putLong(key, value as Long) as T
            Float::class.java -> editor().putFloat(key, value as Float) as T
            else -> editor().putString(key, Gson().toJson(value))
        }
        return this;
    }

    fun hasKey(key: String): Boolean = prefs.contains(key)

    fun remove(key: String): KSharedPrefRaw {
        editor().remove(key)
        return this
    }

    fun apply() {
        editor().apply();
    }
}
