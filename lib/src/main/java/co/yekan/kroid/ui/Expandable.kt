package co.yekan.kroid.ui

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Build
import android.view.View
import android.view.ViewGroup

/**
 * Created by Mo3in on 9/19/2017.
 */

class Expandable(private val targetView: View) {
    private var height: Int = 0;

    fun toggle() {
        change(!isOpen())
    }

    fun open() {
        change(true)
    }

    fun close() {
        change(false)
    }

    fun isOpen(): Boolean {
        return targetView.visibility == View.VISIBLE;
    }

    private fun change(open: Boolean) {

        if (height == 0)
            height = targetView.measuredHeight

        val anim = targetView.animate().setDuration(200).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                if (!open)
                    targetView.visibility = View.GONE;
                else
                    targetView.visibility = View.VISIBLE;
            }
        })

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            anim.setUpdateListener { valueAnimator ->
                val interpolatedTime = valueAnimator.animatedValue as Float

                if (open) {
                    targetView.visibility = View.VISIBLE;

                    if (interpolatedTime == 1f) {
                        targetView.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    } else {
                        targetView.layoutParams.height = (height * interpolatedTime).toInt()
                    }

                } else {
                    if (interpolatedTime == 1f) {
                        targetView.visibility = View.GONE;
                    } else {
                        targetView.layoutParams.height = (height - (height * interpolatedTime)).toInt();
                    }
                }
                targetView.requestLayout();
            }
        }

        anim.start()
    }
}

class ExpandableUtils {
    companion object {
        fun toggle(view1: View, view2: View, clickListener: View.OnClickListener? = null): Expandable {
            val expandable = Expandable(view2);

            view1.setOnClickListener { view ->
                expandable.toggle()
                clickListener?.onClick(view)
            }

            return expandable;
        }
    }
}