package co.yekan.kroid.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import co.yekan.kroid.helpers.LocaleHelper
import co.yekan.kroid.permissionRequest.IPermissionCallBack
import co.yekan.kroid.services.KServiceConfig

/**
 * Created by Mo3in on 12/10/2017.
 */
abstract class KActivity : AppCompatActivity() {

    private val REQUEST_PERMISSION = 1111
    private val NEEDED_PERMISSIONS = 2222

    var pCallback: IPermissionCallBack? = null
    var permissionsNeed: MutableList<String> = mutableListOf<String>()

    open val statusBarDrawable: Int? = null

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        changeStatusColor()
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        changeStatusColor()
        super.onCreate(savedInstanceState, persistentState)
    }

    open fun changeStatusColor(){
        if (statusBarDrawable != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(android.R.color.transparent);
            window.setBackgroundDrawable(resources.getDrawable(statusBarDrawable!!));
        }
    }


    fun requestPermissions(arrays: Array<String>, permissionCallback: IPermissionCallBack) {
        permissionsNeed.clear()
        pCallback = permissionCallback

        arrays.filter { ActivityCompat.checkSelfPermission(applicationContext, it) != PackageManager.PERMISSION_GRANTED }
                .forEach { permissionsNeed.add(it) }

        if (permissionsNeed.size > 0)
            requestNeededPermission(permissionsNeed)
        else
            toast("Permissions Granted")

    }

    fun requestPermissions(permission: String, permissionCallback: IPermissionCallBack) {
        pCallback = permissionCallback

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
                permissionCallback.permissionGranted()
                return
            }

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                ActivityCompat.requestPermissions(this, arrayOf(permission), REQUEST_PERMISSION)
            else
                ActivityCompat.requestPermissions(this, arrayOf(permission), REQUEST_PERMISSION)


        } else
            permissionCallback.permissionGranted()
    }

    private fun requestNeededPermission(permissionsNeed: MutableList<String>) {
        ActivityCompat.requestPermissions(this@KActivity, permissionsNeed.toTypedArray(), NEEDED_PERMISSIONS)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSION) {

            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                pCallback?.permissionGranted()
            else
                pCallback?.permissionDenied()

        } else if (requestCode == NEEDED_PERMISSIONS) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                pCallback?.permissionGranted()
            else
                pCallback?.permissionDenied()

        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase!!, KServiceConfig.getDefaultLang()))
    }
}