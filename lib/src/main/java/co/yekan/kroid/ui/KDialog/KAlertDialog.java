package co.yekan.kroid.ui.KDialog;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Created by Mo3in on 1/6/2017.
 */

public class KAlertDialog extends AlertDialog.Builder {
    public KAlertDialog(Context context) {
        super(context);
    }

    @Override
    public AlertDialog show() {
        AlertDialog dialog = super.show();
        Utils.ChangeDialogFont(getContext(), dialog);
        return dialog;
    }
}
