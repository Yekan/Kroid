package co.yekan.kroid.ui.KDialog

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context

/**
 * Created by Mo3in on 1/23/2018.
 */
class KProgressDialog {
    companion object {
        fun show(context: Context, title: String, message: String): Dialog {
            val dialog = ProgressDialog.show(context, title, message)
            Utils.ChangeDialogFont(context, dialog)

            return dialog
        }
    }
}