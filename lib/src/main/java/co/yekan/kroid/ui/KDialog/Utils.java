package co.yekan.kroid.ui.KDialog;

import android.app.AlertDialog;
import android.content.Context;

import android.view.ViewGroup;

import co.yekan.kroid.ui.KFont;

/**
 * Created by Mo3in on 1/6/2017.
 */

public class Utils {
    public static void ChangeDialogFont(Context context, AlertDialog dialog) {
        try {
            KFont.Companion.instance().replaceFonts(((ViewGroup) dialog.findViewById(android.R.id.message).getParent()).getRootView());
        } catch (Exception e) {

        }
    }
}
