package co.yekan.kroid.ui

import android.content.Context
import android.graphics.Typeface
import android.support.design.widget.TextInputLayout
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.*

/**
 * Created by Mo3in on 9/10/2017.
 */
class KFont {

    private lateinit var defaultFont: String

    fun SetDefaultFont(font: String) {
        defaultFont = font
    }


    fun replaceFonts(view: View) {
        var child: View
        if (view is ViewGroup) {
            val viewTree = view
            for (i in 0 until viewTree.childCount) {
                child = viewTree.getChildAt(i)

                when (child) {
                    is TextView -> replaceViewFont(view.context, child)
                    is TextInputLayout -> replaceTextInputLayoutFont(view.context, child)
                    is ViewGroup -> replaceFonts(child)
                }
            }
        } else if (view is TextView) {
            replaceViewFont(view.context, view)
        }
    }

    private fun replaceViewFont(context: Context, view: TextView) {
        try {
            val fontName = getFontFromAttr(view.tag.toString())
            view.typeface = loadTypeFace(context, fontName)
        } catch (ignored: Exception) {
            view.typeface = loadTypeFace(context, defaultFont)
        }
    }

    private fun replaceTextInputLayoutFont(context: Context, view: TextInputLayout) {
        try {
            val fontName = getFontFromAttr(view.tag.toString())
            view.setTypeface(loadTypeFace(context, fontName))
        } catch (ignored: Exception) {
            view.setTypeface(loadTypeFace(context, defaultFont))
        }
        replaceFonts(view)
    }

    companion object {
        private val typefaces = Hashtable<String, Typeface>()
        private var kTypeFace: KFont? = null

        fun instance(): KFont? {
            if (kTypeFace == null)
                kTypeFace = KFont()

            return kTypeFace
        }

        fun loadTypeFace(context: Context, fontName: String): Typeface? {
            if (!typefaces.containsKey(fontName))
                typefaces.put(fontName, Typeface.createFromAsset(context.assets, "fonts/$fontName.ttf"))

            return typefaces[fontName]
        }

        private fun getFontFromAttr(tag: String): String {

            if (tag.contains("|")) {
                tag.split("\\|".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        .filter { it.startsWith("font:") }
                        .forEach { att -> return att.substring(5).trim { it <= ' ' } }
            } else if (tag.startsWith("font:"))
                return tag.substring(5).trim { it <= ' ' }

            return ""
        }
    }
}

fun View.changeFont() {
    KFont.instance()!!.replaceFonts(this)
}