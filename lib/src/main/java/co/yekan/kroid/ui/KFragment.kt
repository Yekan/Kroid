package co.yekan.kroid.ui

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.View
import co.yekan.kroid.permissionRequest.IPermissionCallBack

/**
 * Created by Mo3in on 9/10/2017.
 */
abstract class KFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.changeFont()
        super.onViewCreated(view, savedInstanceState)
    }


    private val REQUEST_PERMISSION = 1111
    private val NEEDED_PERMISSIONS = 2222

    var pCallback: IPermissionCallBack? = null
    var permissionsNeed: MutableList<String> = mutableListOf<String>()

    fun requestPermissions(arrays: Array<String>, permissionCallback: IPermissionCallBack) {
        permissionsNeed.clear()
        pCallback = permissionCallback

        arrays.filter { ActivityCompat.checkSelfPermission(context!!, it) != PackageManager.PERMISSION_GRANTED }
                .forEach { permissionsNeed.add(it) }

        if (permissionsNeed.size > 0)
            requestNeededPermission(permissionsNeed)
        else
            pCallback?.permissionGranted()

    }

    fun requestPermissions(permission: String, permissionCallback: IPermissionCallBack) {
        pCallback = permissionCallback

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission))
            requestPermissions(arrayOf(permission), REQUEST_PERMISSION)
        else
            requestPermissions(arrayOf(permission), REQUEST_PERMISSION)
    }

    fun requestNeededPermission(permissionsNeed: MutableList<String>) {
        requestPermissions(permissionsNeed.toTypedArray(), NEEDED_PERMISSIONS)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_PERMISSION) {

            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                pCallback?.permissionGranted()
            else
                pCallback?.permissionDenied()

        } else if (requestCode == NEEDED_PERMISSIONS) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                pCallback?.permissionGranted()
            else
                pCallback?.permissionDenied()

        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
