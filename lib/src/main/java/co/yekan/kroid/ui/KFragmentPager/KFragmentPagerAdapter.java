package co.yekan.kroid.ui.KFragmentPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mo3in on 9/8/2017.
 */

public class KFragmentPagerAdapter extends FragmentPagerAdapter {
    private List<KFragmentPagerItem> pagerItems = new ArrayList<>();

    public KFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String title) {
        pagerItems.add(new KFragmentPagerItem(fragment, title));
    }

    public void addFragment(KFragmentPagerItem item) {
        pagerItems.add(item);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pagerItems.get(position).title;
    }

    @Override
    public Fragment getItem(int position) {
        return pagerItems.get(position).fragment;
    }

    @Override
    public int getCount() {
        return pagerItems.size();
    }
}
