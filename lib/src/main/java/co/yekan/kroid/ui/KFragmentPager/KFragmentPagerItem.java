package co.yekan.kroid.ui.KFragmentPager;

import android.support.v4.app.Fragment;

/**
 * Created by Mo3in on 9/8/2017.
 */

public class KFragmentPagerItem {
    public String title;
    public Fragment fragment;

    public KFragmentPagerItem(Fragment fragment, String title) {
        this.title = title;
        this.fragment = fragment;
    }

    public KFragmentPagerItem() {

    }
}
