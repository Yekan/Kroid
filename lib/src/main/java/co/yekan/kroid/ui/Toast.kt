package co.yekan.kroid.ui

import android.content.Context
import android.support.v4.app.Fragment
import android.view.ViewGroup
import android.widget.Toast

/**
 * Created by Mo3in on 9/10/2017.
 */
public class Toast {
    companion object {
        val LENGTH_SHORT = 0
        val LENGTH_LONG = 1

        private fun changeFont(context: Context, toast: Toast): Toast {
            val rootView = toast.view as ViewGroup

            KFont.instance()!!.replaceFonts(rootView)
//        (rootView.getChildAt(0) as TextView).textSize = 14f

            return toast
        }

        fun makeText(context: Context, text: CharSequence, duration: Int): Toast {
            return changeFont(context, Toast.makeText(context, text, duration))
        }

        fun makeText(context: Context, redId: Int, duration: Int): Toast {
            return changeFont(context, Toast.makeText(context, redId, duration))
        }

        fun show(context: Context, message: String) {
            makeText(context, message, LENGTH_SHORT).show()
        }

        fun show(context: Context, resId: Int) {
            makeText(context, resId, LENGTH_SHORT).show()
        }

        fun show(context: Context, message: String, duration: Int) {
            makeText(context, message, duration).show()
        }

        fun show(context: Context, resId: Int, duration: Int) {
            makeText(context, resId, duration).show()
        }
    }
}

fun Context.toast(message: String) {
    co.yekan.kroid.ui.Toast.show(this, message)
}

fun Context.toast(resId: Int) {
    co.yekan.kroid.ui.Toast.show(this, resId)
}

fun Context.longToast(message: String) {
    co.yekan.kroid.ui.Toast.show(this, message, Toast.LENGTH_LONG)
}

fun Context.longToast(resId: Int) {
    co.yekan.kroid.ui.Toast.show(this, resId, Toast.LENGTH_LONG)
}

fun Fragment.toast(message: String) {
    co.yekan.kroid.ui.Toast.show(context!!, message)
}

fun Fragment.toast(resId: Int) {
    co.yekan.kroid.ui.Toast.show(context!!, resId)
}

fun Fragment.longToast(message: String) {
    co.yekan.kroid.ui.Toast.show(context!!, message, Toast.LENGTH_LONG)
}

fun Fragment.longToast(resId: Int) {
    co.yekan.kroid.ui.Toast.show(context!!, resId, Toast.LENGTH_LONG)
}
