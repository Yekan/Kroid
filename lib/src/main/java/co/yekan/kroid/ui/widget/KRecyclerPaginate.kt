package co.yekan.kroid.ui.widget

import android.support.v7.widget.RecyclerView
import io.reactivex.Observable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.StaggeredGridLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

// https://gist.github.com/nesquena/d09dc68ff07e845cc622

class KRecyclerPaginate(val action: (page: Int) -> Unit, val layoutManager: RecyclerView.LayoutManager) : RecyclerView.OnScrollListener() {

    private var visibleThreshold = 5
    private var currentPage = 0

    private var previousTotalItemCount = 0
    private var loading = true
    private val startingPageIndex = 0
    private var itemsTotalCount: Int = 0

    init {
        if (layoutManager is GridLayoutManager)
            visibleThreshold *= layoutManager.spanCount
        if (layoutManager is StaggeredGridLayoutManager)
            visibleThreshold *= layoutManager.spanCount
    }

    fun getLastVisibleItem(lastVisibleItemPositions: IntArray): Int {
        var maxSize = 0
        for (i in lastVisibleItemPositions.indices) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i]
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i]
            }
        }
        return maxSize
    }

    override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
        var lastVisibleItemPosition = 0
        val totalItemCount = layoutManager.itemCount

        when (layoutManager) {
            is StaggeredGridLayoutManager -> {
                val lastVisibleItemPositions = layoutManager.findLastVisibleItemPositions(null)
                lastVisibleItemPosition = getLastVisibleItem(lastVisibleItemPositions)
            }
            is GridLayoutManager -> lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
            is LinearLayoutManager -> lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
        }

        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex
            this.previousTotalItemCount = totalItemCount
            if (totalItemCount == 0) {
                this.loading = true
            }
        }

        if (loading && totalItemCount > previousTotalItemCount) {
            loading = false
            previousTotalItemCount = totalItemCount
        }

        if (!loading && lastVisibleItemPosition + visibleThreshold > totalItemCount && itemsTotalCount > totalItemCount) {
            currentPage++
            onLoadMore(currentPage)
            loading = true
        }
    }

    fun resetState() {
        this.currentPage = this.startingPageIndex
        this.previousTotalItemCount = 0
        this.loading = true
    }

    // Defines the process for actually loading more data based on page
    fun onLoadMore(page: Int) {
        action.invoke(page)
    }

}


data class PageResult<T>(val totalCount: Int, val items: Array<T>)

fun RecyclerView.onLoadMore(action: (page: Int) -> Unit): PaginationController {
    val paginate = KRecyclerPaginate(action, layoutManager)
    addOnScrollListener(paginate)
    return PaginationController(paginate)
}

//fun <TAdapter : KRecyclerAdapter<TModel>, TModel : Any> RecyclerView.setSimplePagination(_adapter: TAdapter, action: (page: Int) -> Observable<PageResult<TModel>>): PaginationController<TAdapter, TModel> {
//    adapter = _adapter
//
//    val paginate = KRecyclerPaginate(_adapter, action, layoutManager)
//    addOnScrollListener(paginate)
//    return PaginationController(_adapter, paginate)
//}
//

class PaginationController(val paginate: KRecyclerPaginate) {

    fun fetchFirst() {
        paginate.onLoadMore(1)
    }

    fun clearAndFetch() {
        clear()
        fetchFirst()
    }

    fun clear() {
        paginate.resetState()
    }
}