package co.yekan.kroid.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import co.yekan.kroid.R
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit


class NumberPicker : LinearLayout {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    var txt_number: EditText? = null

//    private val onChangeEmitter = PublishSubject.create<Int>()
//    lateinit var onChange: Observable<Int>

    var minValue = 1
    var maxValue = 999
    var debounceTime = 300

    var onChangeListener: OnChangeListener? = null

    private var layoutRes: Int = R.layout.number_picker

    private var _value: Int = minValue
    var value: Int
        set(value) {
            val filteredValue = getFilterValue(value)
            if (_value != filteredValue || txt_number != null && txt_number!!.text.isEmpty()) {
                _value = getFilterValue(value)
                txt_number?.setText(_value.toString())
//                onChangeEmitter.onNext(value)
                onChangeListener?.onChange(value)
            }
        }
        get() = _value

    private val obsDisposable = CompositeDisposable()


    private fun init(attrs: AttributeSet) {
        val attributes = context.theme.obtainStyledAttributes(attrs, R.styleable.NumberPicker, 0, 0)

        minValue = attributes.getInteger(R.styleable.NumberPicker_min, 1)
        maxValue = attributes.getInteger(R.styleable.NumberPicker_max, 999)
        value = attributes.getInteger(R.styleable.NumberPicker_value, minValue)
        debounceTime = attributes.getInteger(R.styleable.NumberPicker_debounce_time, 300)
        layoutRes = attributes.getResourceId(R.styleable.NumberPicker_custom_layout, R.layout.number_picker)

        val view = View.inflate(context, layoutRes, this)

        val btn_decrease = view.findViewById<Button>(R.id.btn_decrease)
        val btn_increase = view.findViewById<Button>(R.id.btn_increase)
        txt_number = view.findViewById(R.id.txt_number)

        btn_increase.setOnClickListener {
            value++
        }

        btn_decrease.setOnClickListener {
            value--
        }


        txt_number!!
                .textChanges()
                .debounce(debounceTime.toLong(), TimeUnit.MILLISECONDS)
                .map { t ->
                    getFilterValue(if (t.isEmpty()) 0 else t.toString().toInt())
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    value = it
                }
    }

    private fun getFilterValue(t: Int): Int {
        return when {
            t < minValue -> minValue
            t > maxValue -> maxValue
            else -> t
        }
    }
}

public interface OnChangeListener {
    fun onChange(value: Int)
}