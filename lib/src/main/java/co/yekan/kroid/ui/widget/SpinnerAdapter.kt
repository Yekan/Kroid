package co.yekan.kroid.ui.widget

import android.content.Context
import android.support.annotation.LayoutRes
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import co.yekan.kroid.ui.KFont

/**
 * Created by Mo3in on 9/10/2017.
 */
public open class SpinnerAdapter<T> : ArrayAdapter<T> {

    constructor(context: Context, @LayoutRes resource: Int) : super(context, resource)
    constructor(context: Context) : super(context, android.R.layout.simple_dropdown_item_1line)
    constructor(context: Context, resource: Int, items: List<T>) : super(context, resource, items)
    constructor(context: Context, items: List<T>) : super(context, android.R.layout.simple_dropdown_item_1line, items)


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v = super.getView(position, convertView, parent)
        KFont.instance()!!.replaceFonts(v)
        return v
    }


    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v = super.getDropDownView(position, convertView, parent)
        KFont.instance()!!.replaceFonts(v)
        return v
    }
}
