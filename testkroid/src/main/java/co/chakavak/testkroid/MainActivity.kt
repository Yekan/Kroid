package co.chakavak.testkroid

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import co.yekan.kroid.extentions.KHttpFailed
import co.yekan.kroid.extentions.log
import co.yekan.kroid.services.KServiceConfig
import co.yekan.kroid.services.user.AuthService
import co.yekan.kroid.services.user.IRegisterCallBack
import co.yekan.kroid.services.user.IRegisterModel
import co.yekan.kroid.services.user.auth.AuthConfig
import co.yekan.kroid.services.user.auth.setAuthConfig
import co.yekan.kroid.ui.toast

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		KServiceConfig.setHttpEndPoint("http://192.168.3.100:5003").setAuthConfig(AuthConfig.Kican("http://192.168.3.100:5003"))

		AuthService(this).register(UserRegisterModel("test@aest.co", "123456a", "123456a", true, 2), object : IRegisterCallBack {
			override fun onSuccess() {
				toast("sadsad");
			}

			override fun onFailed(failed: KHttpFailed) {
				log(failed)
			}

		})

	}
}

data class UserRegisterModel(override val email: String, override val password: String, override val confirmPassword: String, val isMan: Boolean, val countryId: Int) : IRegisterModel